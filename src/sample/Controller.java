package sample;

import javafx.application.Platform;
import javafx.event.Event;

public class Controller {
    public void startTutorial(Event event) {
        TrainWindow trainWindow = new TrainWindow();
        trainWindow.start();
    }

    public void startGame(Event event) {
        GameWindow gameWindow = new GameWindow();
        gameWindow.GameStart();
    }

    public void exit(Event event) {
        Platform.exit();
    }
}
