package sample;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Arrow
{
    private Vector2 position;
    private Vector2 velocity;
    double screenX;
    double screenY;
    int direction;
    int colorIndex;
    Color color;

    Arrow()
    {
        position = new Vector2(0, 0);
        velocity = new Vector2(0, 0);
        color = Color.BLUE;
    }

    Arrow(double x, double y, int dir, Color c, int colorInd, double screenResolutionX, double screenResolutionY)
    {
        position = new Vector2(x, y);
        screenX = screenResolutionX;
        screenY = screenResolutionY;

        colorIndex = colorInd;

        velocity = new Vector2(0, 0);

        double centerX = screenX/2;
        double centerY = screenY/2;

        velocity.x = centerX - position.x;
        velocity.y = centerY - position.y;
        velocity.Normalize();

        direction = dir;

        color = c;
    }

    public void SetPosition(double x, double y)
    {
        position.x = x;
        position.y = y;
    }

    public Vector2 GetPosition()
    {
        Vector2 result = new Vector2(position.x, position.y);
        return result;
    }

    public Vector2 GetVelocity()
    {
        Vector2 result = new Vector2(velocity.x, velocity.y);
        return result;
    }

    public void Update(double t, double speedFactor)
    {
        position.x +=  (3+speedFactor) * velocity.x;
        position.y += (3+speedFactor)* velocity.y;
    }

    public void Render(GraphicsContext gc)
    {
        gc.setFill(color);
        gc.setStroke(color);
        gc.setLineWidth(10);

        double length = 25.0;
        double width = 20.0;
        Vector2 backCenter = new Vector2(position.x, position.y);

        backCenter.x = position.x - (length * velocity.x);
        backCenter.y = position.y - (length * velocity.y);

        Vector2 backToFront = new Vector2(position.x - backCenter.x,
                                          position.y - backCenter.y);
        backToFront.Normalize();
        Vector2 rightPerpendicular = backToFront.Perpendicular();

        Vector2 rightPoint = new Vector2(backCenter.x + (width / 2) * rightPerpendicular.x ,
                                         backCenter.y + (width / 2) * rightPerpendicular.y);

        Vector2 leftPoint = new Vector2(backCenter.x - (width / 2) * rightPerpendicular.x ,
                                        backCenter.y - (width / 2) * rightPerpendicular.y);

        double xC[] = {position.x, rightPoint.x, leftPoint.x};
        double yC[] = {position.y, rightPoint.y, leftPoint.y};

        gc.fillPolygon(xC, yC, 3);

       // gc.strokeRect(position.x, position.y, 10, 20);
    }
};