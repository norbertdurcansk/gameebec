package sample;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;

import java.util.ArrayList;

/**
 * Created by Dusan on 01-Mar-16.
 */
public class Game
{
    double screenX;
    double screenY;

    Label points;
    Box box;

    private ArrayList<Arrow> arrows;
    private ArrayList<Color> colors;
    private ArrayList<Vector2> startingPositions;

    private int purple = 0;
    private int yellow = 1;
    private int blue = 2;
    private int green = 3;

    private int left = 0;
    private int up = 1;
    private int right = 2;
    private int down = 3;

    double previousTime = 0;

    public int score = 0;

    public int stage = 1;

    public double speedFactor = 0.5;

    private double PI = 3.14159265;

    private long startNanoTime;

    Game(double screenResolutionX, double screenResolutionY, Box b, Label l, long startNanoTime)
    {
        points = l;
        box = b;
        this.startNanoTime = startNanoTime;

        colors = new ArrayList<Color>();
        colors.add(Color.rgb(255, 174, 201));   // purple
        colors.add(Color.rgb(255, 242, 0));     // yellow
        colors.add(Color.rgb(153, 217, 234));   // blue
        colors.add(Color.rgb(181, 230, 29));    // green

        screenX = screenResolutionX;
        screenY = screenResolutionY;

        startingPositions = new ArrayList<Vector2>();

        arrows = new ArrayList<Arrow>();

        double rotation = 0;

        double aspectRatio = screenX / screenY;
        double startOffset = screenY / 5;

        for (int i = 0; i < 4; ++i)
        {
            double x = screenX/2 + 3*startOffset * Math.cos(rotation);
            double y = screenY/2 + aspectRatio * startOffset * Math.sin(rotation);

            Vector2 startingPos = new Vector2(x, y);
            startingPositions.add(startingPos);

            int Min = 0;
            int Max = 3;
            int random = Min + (int)(Math.random() * ((Max - Min) + 1));

            Color color = colors.get(random);

            int direction = i;
            Arrow arrow = new Arrow(x, y, direction, color, random, screenX, screenY);
            //arrows.add(arrow);

            rotation += 2*PI / 4;
        }
    }

    public void Update(double t, ArrayList<String> input) {
        double dt = t - previousTime;
        previousTime = t;

        ArrayList<Arrow> tmp = new ArrayList<Arrow>();

        for (int i = 0; i < arrows.size(); ++i) {
            boolean valid = true;
            Arrow arrow = arrows.get(i);
            arrow.Update(t, speedFactor);

            Vector2 arrowPos = arrow.GetPosition();

            double upperBound = screenY / 2 - box.GetHeight() / 2;
            double lowerBound = screenY / 2 + box.GetHeight() / 2;
            double rightBound = screenX / 2 + box.GetWidth() / 2;
            double leftBound = screenX / 2 - box.GetWidth() / 2;

            //System.out.printf("%f\n", arrowPos.x);

            if (arrow.direction == left)
            {
                if (arrowPos.x < rightBound)
                {
                    if (arrow.colorIndex == box.rightColor)
                    {
                        score += 100;
                        if (score % 500 == 0) stage++;
                    }
                    valid = false;
                }
            }
            if (arrow.direction == up)
            {
                if (arrowPos.y < lowerBound)
                {
                    if (arrow.colorIndex == box.bottomColor)
                    {
                        score += 100;
                        if (score % 500 == 0) stage++;
                    }
                    valid = false;
                }
            }
            if (arrow.direction == right)
            {
                if (arrowPos.x > leftBound)
                {
                    if (arrow.colorIndex == box.leftColor)
                    {
                        score += 100;
                        if (score % 500 == 0) stage++;
                    }
                    valid = false;
                }
            }
            if (arrow.direction == down)
            {
                if (arrowPos.y > upperBound)
                {
                    if (arrow.colorIndex == box.topColor)
                    {
                        score += 100;
                        if (score % 500 == 0) stage++;
                    }
                    valid = false;
                }
            }

            if (valid)
                tmp.add(arrow);

        }
        arrows = tmp;


        if (arrows.size() == 0)
        {
            int Min = 0;
            int Max = 3;
            int random = Min + (int)(Math.random() * ((Max - Min) + 1));

            Color color = colors.get(random);

            int random2 = Min + (int)(Math.random() * ((Max - Min) + 1));
            Vector2 startingPos = startingPositions.get(random2);
            double x = startingPos.x;
            double y = startingPos.y;

            Arrow arrow = new Arrow(x, y, random2, color, random, screenX, screenY);
            arrows.add(arrow);
        }

        points.setText("STAGE:" + stage + " POINTS: " + score);
    }

    private double getSpeedFactor() {
        float speedDecision = System.currentTimeMillis() - startNanoTime;
        if (speedDecision <= 1_000) {
            return 0.5d;
        }
        else if (speedDecision <= 2_000) {
            return 0.5d;
        }
        else {
            return 0.5d;
        }
    }

    public void Render(GraphicsContext gc)
    {
        for (int i = 0; i < arrows.size(); ++i)
        {
            arrows.get(i).Render(gc);
        }


    }
}
