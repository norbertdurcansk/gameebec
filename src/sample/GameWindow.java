package sample;

import javafx.animation.AnimationTimer;
import javafx.animation.RotateTransition;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by noro on 1.3.2016.
 */
public  class GameWindow {

    public Label points;

    public  void  GameStart()  {
        Stage theStage= new Stage();
        points = new Label("STAGE: 0 POINTS: 0");
        points.setTextFill(Color.WHITE);
        points.setFont(new Font("Arial", 30));
        Box box = new Box();
        box.SetImage(new Image("img1.png"));

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double screenX = screenSize.getWidth();
        double screenY = screenSize.getHeight();

//        int screenX = 1080;
//        int screenY = 720;

        Group root = new Group();
        Scene theScene = new Scene(root,screenX,screenY);

        theStage.setScene(theScene);
        theStage.setFullScreen(true);

        theStage.show();

        Canvas canvas = new Canvas(screenX, screenY);
        root.getChildren().add(canvas);

        GraphicsContext gc = canvas.getGraphicsContext2D();

        ArrayList<String> input = new ArrayList<String>();

        theScene.setOnKeyPressed(
                new EventHandler<KeyEvent>() {
                    public void handle(KeyEvent e) {
                        String code = e.getCode().toString();

                        // only add once... prevent duplicates
                        if (!input.contains(code))
                            input.add(code);
                    }
                });

        theScene.setOnKeyReleased(
                new EventHandler<KeyEvent>() {
                    public void handle(KeyEvent e) {
                        String code = e.getCode().toString();
                        input.remove(code);
                    }
                });


        final long startNanoTime = System.nanoTime();
        final int angle = 0;
    VBox vbox1 = new VBox();
        vbox1.setLayoutX(theScene.getWidth()-500);
        vbox1.setLayoutY(0);
        ((Group) theScene.getRoot()).getChildren().add(vbox1);
        vbox1.getChildren().add(points);

        VBox vbox = new VBox();
        vbox.setLayoutX(theScene.getWidth() / 2 - 50);
        vbox.setLayoutY(theScene.getHeight() / 2 - 50);
        ((Group) theScene.getRoot()).getChildren().add(vbox);

        Rectangle rect = new Rectangle(100, 40, 100, 100);
        ImagePattern pattern = new ImagePattern(box.image);


        rect.setArcHeight(10);
        rect.setArcWidth(10);
        rect.setFill(pattern);
        RotateTransition rt = new RotateTransition(Duration.millis(100), rect);
        vbox.getChildren().add(rect);
        vbox.setSpacing(10);


        Game game = new Game(screenX, screenY, box, points, startNanoTime);

        new AnimationTimer() {
            public void handle(long currentNanoTime) {
                gc.setFill(Color.BLACK);
                gc.fillRect(0,0,screenX,screenY);

                double t = (currentNanoTime - startNanoTime) / 1000000000.0;
                int previousstage=game.stage;
                game.Update(t, input);
                game.Render(gc);
                if(game.stage!=previousstage && game.speedFactor!=1)
                {
                    game.speedFactor+= 0.2;
                    previousstage=game.stage;

                }

                if (input.contains("LEFT")) {
                    input.clear();
                    box.setAngle(box.getAngle()-90);
                    if(box.getAngle()==-360)
                        box.setAngle(0);
                    rt.setByAngle(-90);
                    rt.setCycleCount(1);
                    rt.play();

                } else if (input.contains("RIGHT")) {
                    input.clear();
                    box.setAngle(box.getAngle()+90);
                    if (box.getAngle() == 360)
                        box.setAngle(0);
                    rt.setByAngle(90);
                    rt.setCycleCount(1);
                    rt.play();
                }
            }
        }.start();
    }

    }
