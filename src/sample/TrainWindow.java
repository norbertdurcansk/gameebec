package sample;

import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;

import java.awt.*;
import java.io.File;

/**
 * Created by kocopepo on 01.03.16.
 */
public class TrainWindow {
    public void start() {
        Stage stage = new Stage();
        String workdir = System.getProperty("user.dir");
        File f = new File(workdir, "res/tutorial.mp4");

        Media video = new Media(f.toURI().toString());
        MediaPlayer mp = new MediaPlayer(video);
        MediaView mv = new MediaView(mp);

        StackPane root = new StackPane();
        root.getChildren().add(mv);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double screenX = screenSize.getWidth();
        double screenY = screenSize.getHeight();
        stage.setFullScreen(true);
        Scene scene =new Scene(root, screenX, screenY);
        stage.setScene(scene);
        stage.setTitle("Training");
        stage.show();
        mp.play();
    }
}
