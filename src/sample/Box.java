package sample;

import javafx.scene.image.Image;

/**
 * Created by kocopepo on 01.03.16.
 */
public class Box{

    private int purple = 0;
    private int yellow = 1;
    private int blue = 2;
    private int green = 3;

    int topColor = purple;
    int rightColor = green;
    int leftColor = yellow;
    int bottomColor = blue;

    public int getAngle() {
        return angle;
    }

    public void setAngle(Integer angle)
    {
        this.angle = angle;

        if (angle == 0)
        {
            topColor = purple;
            rightColor = green;
            leftColor = yellow;
            bottomColor = blue;
        }
        else if (angle == 90 || angle == -270)
        {
            topColor = yellow;
            rightColor = purple;
            leftColor = blue;
            bottomColor = green;
        }
        else if (angle == 180 || angle == -180)
        {
            topColor = blue;
            rightColor = yellow;
            leftColor = green;
            bottomColor = purple;
        }
        else if (angle == 270 || angle == -90)
        {
            topColor = green;
            rightColor = blue;
            leftColor = purple;
            bottomColor = yellow;
        }

    }

    public void SetImage( Image i) {
        image = i;
        width = 100;//i.getWidth();
        height = 100;//i.getHeight();
    }

    double GetWidth () { return width; }
    double GetHeight () { return height; }

    int GetTopColor()
    {
        if (angle == 0)
            return purple;
        else if (angle == 90)
            return yellow;
        else if (angle == 180)
            return blue;
        else if (angle == 270)
            return green;
        else
            return -1;
    }

    public int angle;
    public Image image;
    private double width;
    private double height;
}
