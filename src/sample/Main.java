package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.awt.*;

public class Main extends Application {

    @Override
    public void start(Stage theStage) throws Exception{

        theStage.setTitle( "EbecCube" );
        Parent root = FXMLLoader.load(getClass().getResource("menu.fxml"));
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double screenX = screenSize.getWidth();
        double screenY = screenSize.getHeight();
        theStage.setFullScreen(true);
        Scene scene =new Scene(root, screenX, screenY);
        theStage.setScene(scene);

        theStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}