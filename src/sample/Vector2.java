package sample;

/**
 * Created by Dusan on 01-Mar-16.
 */
public class Vector2 {
    double x = 0;
    double y = 0;

    Vector2(Vector2 v)
    {
        x = v.x;
        y = v.y;
    }

    Vector2(double xVal, double yVal)
    {
        x = xVal;
        y = yVal;
    }

    public void Add(double xVal, double yVal)
    {
        x += xVal;
        y += yVal;
    }

    public void Add(Vector2 v)
    {
        x += v.x;
        y += v.y;
    }

    public void Subtract(double xVal, double yVal)
    {
        x -= xVal;
        y -= yVal;
    }

    public void Subtract(Vector2 v)
    {
        x -= v.x;
        y -= v.y;
    }

    public Vector2 Perpendicular()
    {
        Vector2 v = new Vector2(y, -x);
        return v;
    }

    public void Normalize()
    {
        double length = java.lang.Math.sqrt(x*x + y*y);
        x = x / length;
        y = y / length;
    }

    public double DotProduct(Vector2 v)
    {
        double result = x*v.x + y*v.y;
        return result;
    }
}
